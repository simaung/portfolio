import React from "react";
import { motion } from "framer-motion";
import { EnvelopeIcon, MapPinIcon, PhoneIcon } from "@heroicons/react/24/solid";
import { PageInfo } from "../typings";

type Props = {
  pageInfo: PageInfo;
};

export default function Contact({ pageInfo }: Props) {
  return (
    <motion.div
      initial={{ opacity: 0 }}
      transition={{ duration: 1.5 }}
      whileInView={{ opacity: 1 }}
      className="relative h-screen flex flex-col text-left md:flex-row items-center justify-evenly mx-auto max-w-full overflow-hidden px-10"
    >
      <h2 className="absolute top-20 uppercase tracking-[10px] md:tracking-[20px] text-gray-500 text-xl md:text-2xl">
        Contact
      </h2>
      <div className="flex flex-col space-y-10">
        <h4 className="text-2xl md:text-4xl font-semibold text-center">
          I have got just what you need.{" "}
          <span className="underline decoration-[#F7AB0A]/50">Lets Talk.</span>
        </h4>
        <div className="space-y-10">
          <div className="flex  space-x-5 justify-center">
            <PhoneIcon className="h-7 w-7 text-[#F7AB0A]/50 animate-pulse" />
            <p className="text-lg md:text-2xl">{pageInfo?.phoneNumber}</p>
          </div>
          <div className="flex  space-x-5 justify-center">
            <EnvelopeIcon className="h-7 w-7 text-[#F7AB0A]/50 animate-pulse" />
            <p className="text-lg md:text-2xl">{pageInfo?.email}</p>
          </div>
          <div className="flex space-x-5 justify-center">
            <MapPinIcon className="h-7 w-7 text-[#F7AB0A]/50 animate-pulse" />
            <p className="text-lg md:text-2xl">{pageInfo?.address}</p>
          </div>
        </div>
      </div>
    </motion.div>
  );
}
