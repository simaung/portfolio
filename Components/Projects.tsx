/* eslint-disable @next/next/no-img-element */
import React from "react";
import { motion } from "framer-motion";
import Link from "next/link";
import { Project } from "../typings";
import { urlFor } from "../sanity";

type Props = {
  projects: Project[];
};

export default function Projects({ projects }: Props) {
  return (
    <motion.div
      initial={{ opacity: 0 }}
      transition={{ duration: 1.5 }}
      whileInView={{ opacity: 1 }}
      className="relative h-screen flex flex-col text-left md:flex-row items-center justify-evenly mx-auto max-w-full overflow-hidden px-10"
    >
      <h2 className="absolute top-20 uppercase tracking-[10px] md:tracking-[20px] text-gray-500 text-xl md:text-2xl">
        Projects
      </h2>
      <div className="absolute w-full flex overflow-x-scroll overflow-y-hidden snap-x snap-mandatory z-20 scrollbar-thin scrollbar-thumb-[#F7AB0A] top-24 md:top-0">
        {projects.map((project, i) => (
          <div
            key={project._id}
            className="flex flex-col flex-shrink-0 w-full h-screen snap-center space-y-5 items-center py-20 px-5 md:py-36 md:px-10"
          >
            <motion.img
              initial={{ y: -200, opacity: 0 }}
              transition={{ duration: 1.5 }}
              whileInView={{ y: 0, opacity: 1 }}
              viewport={{ once: true }}
              className="w-[200px] md:w-[300px] xl:w-[450px] rounded-2xl"
              src={urlFor(project?.image).url()}
              alt=""
            />
            <div className="space-y-4 px-0 md:px-10 max-w-6xl">
              <motion.h4
                initial={{ opacity: 0 }}
                transition={{ duration: 2.5 }}
                whileInView={{ opacity: 1 }}
                viewport={{ once: true }}
                className="text-xl md:text-2xl font-semibold text-center"
              >
                <Link
                  className="text-[#F7AB0A]/80 hover:text-[#F7AB0A]"
                  href={project?.linkToBuild ? project?.linkToBuild : "#"}
                  target={"blank"}
                >
                  {i + 1}. {project.title}
                </Link>
              </motion.h4>
              <motion.div
                initial={{ opacity: 0 }}
                transition={{ duration: 2.5 }}
                whileInView={{ opacity: 1 }}
                viewport={{ once: true }}
                className="flex items-center space-x-2 justify-center"
              >
                {project.technologies.map((technology) => (
                  <img
                    className="w-6 h-6 md:w-8 md:h-8 lg:w-10 lg:h-10"
                    key={technology._id}
                    src={urlFor(technology.image).url()}
                    alt=""
                  />
                ))}
              </motion.div>
              <motion.p
                initial={{ y: 100, opacity: 0 }}
                transition={{ duration: 1.5 }}
                whileInView={{ y: 0, opacity: 1 }}
                viewport={{ once: true }}
                className="text-center md:text-left text-sm md:text-lg"
              >
                {project?.summary}
              </motion.p>
            </div>
          </div>
        ))}
      </div>
      <div className="w-full absolute top-[30%] h-[400px] bg-[#F7AB0A]/10 -skew-y-12" />
    </motion.div>
  );
}
