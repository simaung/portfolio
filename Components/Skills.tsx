/* eslint-disable @next/next/no-img-element */
import React from "react";
import { motion } from "framer-motion";
import Skill from "./Skill";
import { Skill as SkillType } from "../typings";

type Props = {
  skills: SkillType[];
};

export default function Skills({ skills }: Props) {
  return (
    <motion.div
      initial={{ opacity: 0 }}
      transition={{ duration: 1.5 }}
      whileInView={{ opacity: 1 }}
      className="relative h-screen flex flex-col text-center md:flex-row md:text-left items-center justify-evenly mx-auto max-w-7xl px-10"
    >
      <h2 className="absolute top-20 uppercase tracking-[10px] md:tracking-[20px] text-gray-500 text-xl md:text-2xl">
        Skills
      </h2>
      <div className="grid grid-cols-4 gap-5">
        {skills?.slice(0, skills.length / 2).map((skill) => (
          <Skill key={skill._id} skill={skill} />
        ))}

        {skills?.slice(skills.length / 2, skills.length).map((skill) => (
          <Skill key={skill._id} skill={skill} directionLeft/>
        ))}
      </div>
    </motion.div>
  );
}
