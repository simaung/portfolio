/* eslint-disable @next/next/no-img-element */
import React from "react";
import { motion } from "framer-motion";
import ExperienceCard from "./ExperienceCard";
import { Experience as ExperienceType } from "../typings";

type Props = {
  experiences: ExperienceType[];
};

export default function Experience({ experiences }: Props) {
  return (
    <motion.div
      initial={{ opacity: 0 }}
      transition={{ duration: 1.5 }}
      whileInView={{ opacity: 1 }}
      className="relative h-screen flex flex-col text-center md:flex-row md:text-left items-center justify-evenly mx-auto max-w-7xl px-10"
    >
      <h2 className="absolute top-20 uppercase tracking-[10px] md:tracking-[20px] text-gray-500 text-xl md:text-2xl">
        Experience
      </h2>
      <div className="w-full flex space-x-5 overflow-x-scroll p-10 snap-x snap-mandatory scrollbar-thin scrollbar-thumb-[#F7Ab0A] mt-10">
        {experiences?.map((experience) => (
          <ExperienceCard key={experience._id} experience={experience} />
        ))}
      </div>
    </motion.div>
  );
}
