/* eslint-disable @next/next/no-img-element */
import React from "react";
import { motion } from "framer-motion";
import { Skill } from "../typings";
import { urlFor } from "../sanity";

type Props = {
  skill: Skill;
  directionLeft?: boolean;
};

function Skill({ skill, directionLeft }: Props) {
  return (
    <div className="group relative flex flex-row cursor-pointer">
      <motion.img
        initial={{ opacity: 0, x: directionLeft ? -100 : 100 }}
        transition={{ duration: 1.5 }}
        whileInView={{ opacity: 1, x: 0 }}
        viewport={{ once: true }}
        src={urlFor(skill?.image).url()}
        className="w-12 h-12 lg:w-24 lg:h-24 rounded-full border border-gray-500 object-cover group-hover:grayscale transition duration-300 ease-in-out"
      />
    </div>
  );
}

export default Skill;
