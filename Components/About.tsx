/* eslint-disable @next/next/no-img-element */
import React from "react";
import { motion } from "framer-motion";
import { urlFor } from "../sanity";
import { PageInfo } from "../typings";

type Props = {
  pageInfo: PageInfo;
};

export default function About({ pageInfo }: Props) {
  return (
    <motion.div
      initial={{ opacity: 0 }}
      transition={{ duration: 1.5 }}
      whileInView={{ opacity: 1 }}
      className="relative h-screen flex flex-col text-center md:flex-row md:text-left  items-center justify-evenly mx-auto max-w-7xl px-10"
    >
      <h2 className="absolute top-20 uppercase tracking-[10px] md:tracking-[20px] text-gray-500 text-xl md:text-2xl">
        About
      </h2>
      <motion.img
        initial={{ x: -200, opacity: 0 }}
        transition={{ duration: 1.2 }}
        whileInView={{ x: 0, opacity: 1 }}
        viewport={{ once: true }}
        className="-mb-32 md:mb-0 flex-shrink-0 w-40 h-40 rounded-full object-cover md:rounded-lg md:w-[250px] md:h-[300px] xl:w-[350px] xl:h-[450px] xl:mt-24"
        src={urlFor(pageInfo?.profilePic).url()}
        alt=""
      />
      <div className="space-y-5 px-0 md:px-10">
        <h4 className="text-2xl md:text-4xl font-semibold">
          Here is a{" "}
          <span className="underline decoration-[#F7AB0A]/50">little</span>{" "}
          background
        </h4>
        <div
          className="text-sm md:text-base"
          dangerouslySetInnerHTML={{ __html: pageInfo?.backgroundInformation }}
        ></div>
      </div>
    </motion.div>
  );
}
